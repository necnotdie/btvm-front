import Index from '@/components/Index';
import Home from '@/components/home/Home'

const routes = [
    {
        path: '/',
        name: '京视传媒',
        component: Index,
        meta: {
            requireAuth: true
        },
        children: [
            {
                path: 'home',
                component: Home,
                name: 'home',
                title: 'home'
            }
        ]
    }
]
export default routes
