import Vue from 'vue'
import App from './App.vue'
import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.min'
import router from "./router";
import 'font-awesome/css/font-awesome.min.css'
import '@/assets/js/owl-carousel/owl.carousel.min'
import '@/assets/js/owl-carousel/owl.carousel.css'
import '@/assets/js/owl-carousel/owl.theme.css'
import '@/assets/css/style.css'

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
    $
}).$mount('#app')
