import Message from '@/plugins/message'
import $ from 'jquery'

const defaultConfig = {
    type: "get",
    async: true
}

export function ajaxPromise(param) {
    return new Promise((resovle, reject) => {
        $.ajax({
            ...param,
            success: res => {
                if (res.code !== 200 && res.code !== '200') {
                    if (res.code === 300 || res.code === '300') {
                        console.log(res.data);
                        window.location.href = res.data;
                    } else {
                        Message({
                            type: 'error',
                            message: res.message,
                            duration: 2000,
                            offset: 200
                        });
                        reject(res);
                    }
                } else {
                    resovle(res.data);
                }
            },
            error: error => {
                if (error) {
                    let errorMessage;
                    switch (error.status) {
                        case 404:
                            errorMessage = '网络请求不存在';
                            break;
                        case 500:
                            errorMessage = '服务器开小差了，请稍后重试';
                            break;
                        case 502:
                            errorMessage = '抱歉，服务暂时不可用，请稍后重试';
                            break;
                        case 503:
                            errorMessage = '抱歉，服务暂时不可用，请稍后重试';
                            break;
                        case 401:
                            errorMessage = '请重新登录！';
                            break;
                        default:
                            errorMessage = '系统繁忙，请稍后重试';
                    }
                    Message({
                        type: 'error',
                        message: errorMessage,
                        duration: 2000,
                        offset: 200
                    });
                } else {
                    if (error.statusText.includes('timeout')) {
                        Message({
                            type: 'error',
                            message: '请求超时，请检查网络是否连接正常',
                            duration: 2000,
                            offset: 200
                        });
                    } else {
                        Message({
                            type: 'error',
                            message: '请求失败，请检查网络是否已连接',
                            duration: 2000,
                            offset: 200
                        });
                    }
                }
                reject(error);
            }
        })
    })
}

async function ajaxHttp(body, head, config) {
    let option = $.extend({}, defaultConfig, config);
    const promise = await ajaxPromise({
        ...option,
        data: body
    });
    return promise;
}

export default function request(config) {
    return (body = {}, head = {}) => {
        const result = ajaxHttp(body, head, config);
        return result;
    };
}