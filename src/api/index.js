import request from "@/utils/request";

export const login = request({
    url: "/btvm-front-api/login",
    type: "get"
})